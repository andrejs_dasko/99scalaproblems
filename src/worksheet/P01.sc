package worksheet

import problem.Last._

object P01 {
  val list = List(1, 1, 2, 3, 5, 8)               //> list  : List[Int] = List(1, 1, 2, 3, 5, 8)

  last(list)                                      //> res0: Int = 8
  last2(list)                                     //> res1: Int = 8
  last3(list)                                     //> res2: Int = 8
  last4(list)                                     //> res3: Int = 8
  // last(List())
  // last2(List())
	// last3(List())
}