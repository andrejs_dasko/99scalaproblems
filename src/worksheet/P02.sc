package worksheet

import problem.Penultimate._

object P02 {
  val list = List(1, 1, 2, 3, 5, 8)               //> list  : List[Int] = List(1, 1, 2, 3, 5, 8)

  penultimateBuiltIn(List(1, 2, 3, 4, 7, 11))     //> res0: Int = 7
  
  penultimateRecursive(list)                      //> res1: Int = 5
  penultimateRecursive(List(1,2))                 //> res2: Int = 1
}