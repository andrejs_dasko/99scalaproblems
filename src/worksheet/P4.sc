package worksheet

import problem.Length._

object P4 {
  val list = List(1, 1, 2, 3, 5, 8)               //> list  : List[Int] = List(1, 1, 2, 3, 5, 8)
  lengthBuiltIn(list)                             //> res0: Int = 6
  
  lengthRecursive(list)                           //> res1: Int = 6
  lengthRecursive(List())                         //> res2: Int = 0
  lengthRecursive(List(2))                        //> res3: Int = 1
  
  lengthTailRecursive(list)                       //> res4: Int = 6
  lengthTailRecursive(List())                     //> res5: Int = 0
  lengthTailRecursive(List(2))                    //> res6: Int = 1
}