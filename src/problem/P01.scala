package problem

object P01 {
  import Last._
  def main(args: Array[String]) {
    val list = List(1, 1, 2, 3, 5, 8)
    assert(last(list) == 8)
    assert(last2(list) == 8)
    assert(last3(list) == 8)
  }
}

object Last {
  def last[T](list: List[T]): T = list.last //> last: [T](list: List[T])T

  def last2[T](list: List[T]): T = {
    if (list.isEmpty) throw new NoSuchElementException
    else if (list.size == 1) list.head
    else last2(list.tail)
  }

  def last3[T](list: List[T]): T = list match {
    case List() => throw new NoSuchElementException
    case List(x) => x
    case _ => last3(list.tail)
  }

  /*
  *  Official solution
  */
  def last4[T](list: List[T]): T = list match {
    case x :: Nil => x
    case _ :: tail => last4(tail)
    case _ => throw new NoSuchElementException
  }
}