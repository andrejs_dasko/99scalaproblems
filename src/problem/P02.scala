package problem

/*
 * Find the last but one element of a list.
 * Example:
 * scala> penultimate(List(1, 1, 2, 3, 5, 8))
 * res0: Int = 5 
 */
object P02 {
  def main(args: Array[String]) {
    import Penultimate._
    val list = List(1, 1, 2, 3, 5, 8)
    assert(penultimateRecursive(list) == 5)
  }
}

object Penultimate {
  def penultimateBuiltIn[T](list: List[T]): T = list.init.last
  
  def penultimateRecursive[T](list: List[T]): T = list match {
    case x :: y :: Nil => x
    case _ :: tail => penultimateRecursive(list.tail)
    case _ => throw new NoSuchElementException
  }
  
  def penultimateOfficial[A](ls: List[A]): A = ls match {
    case h :: _ :: Nil => h
    case _ :: tail     => penultimateOfficial(tail)
    case _             => throw new NoSuchElementException
  }
}