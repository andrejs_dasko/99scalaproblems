package problem

/**
 * Find the number of elements of a list.
 * Example:
 * scala> length(List(1, 1, 2, 3, 5, 8))
 * res0: Int = 6
 */
object P04 {
  def main(args: Array[String]) {

  }
}

object Length {
  def lengthBuiltIn[T](list: List[T]): Int = list.length

  def lengthRecursive[T](list: List[T]): Int = list match {
    case Nil => 0
    case _ => 1 + lengthRecursive(list.tail)
  }

  def lengthTailRecursive[T](list: List[T]): Int = {
    def loop[T](l: List[T], acc: Int): Int = l match {
      case Nil => acc
      case _ => loop(l.tail, acc + 1)
    }
    loop(list, 0)
  }

  // Tail recursive solution.  Theoretically more efficient; with tail-call
  // elimination in the compiler, this would run in constant space.
  // Unfortunately, the JVM doesn't do tail-call elimination in the general
  // case. Scala *will* do it if the method is either final or is a local
  // function.  In this case, `lengthR` is a local function, so it should
  // be properly optimized.
  // For more information, see
  // http://blog.richdougherty.com/2009/04/tail-calls-tailrec-and-trampolines.html
  def lengthTailRecursiveOfficial[A](ls: List[A]): Int = {
    def lengthR(result: Int, curList: List[A]): Int = curList match {
      case Nil => result
      case _ :: tail => lengthR(result + 1, tail)
    }
    lengthR(0, ls)
  }

  // More pure functional solution, with folds.
  def lengthFunctionalOfficial[A](ls: List[A]): Int = ls.foldLeft(0) { (c, _) => c + 1 }
}